import React from 'react';
import './DiwaliGrid.css';

/* 
MIT License

https://gitlab.com/mtiberi-open/diwali-grid

Copyright (c) 2018-present, Mauro Tiberi <mauro.tiberi@live.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

class DiwaliGrid extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            columnWidths: props.columns.map(x => x.initialWidth || DiwaliGrid.defaults.initialWidth),
            selectedRow: props.selectedRow || -1,
            sortIndex: this.getInitialSortIndex(),
        }

        this.clickCell = (colIndex, rowIndex) => {
            this.setState({ selectedRow: rowIndex })
            const { clickCell } = this.props
            if (clickCell)
                clickCell(colIndex, rowIndex);
        }

        this.updateWidths = () => {
            const columnWidths = this.nextWidths;
            this.nextWidths = undefined
            this.setState({ columnWidths })
        }

        this.setColumnWidth = (columnIndex, width) => {

            const { minWidth = DiwaliGrid.defaults.minWidth } = this.props

            if (width < minWidth)
                width = minWidth

            if (this.nextWidths === undefined) {
                setTimeout(this.updateWidths, DiwaliGrid.defaults.delay)
                this.nextWidths = this.state.columnWidths.slice()
            }
            this.nextWidths[columnIndex] = width;
        }

        this.updateFirstRow = () => {
            const firstRow = this.nextFirstRow;
            this.nextFirstRow = undefined;
            this.setState({ firstRow })
        }

        this.setFirstRow = firstRow => {
            if (this.nextFirstRow === undefined)
                setTimeout(this.updateFirstRow, DiwaliGrid.defaults.delay)
            this.nextFirstRow = firstRow
        }


        this.sortByColumn = sortColumn => {
            let state = this.state
            const sortDescending = sortColumn === state.sortColumn
                ? !state.sortDescending
                : false

            const sortIndex = this.getSortIndex(sortColumn, sortDescending)

            this.setState({ sortIndex, sortDescending, sortColumn })
        }
    }

    getSortIndex(sortColumn, sortDescending) {

        const { comparers, columns } = this.props
        const column = columns[sortColumn]
        let values = column.values

        let compare = (va, vb) => va > vb ? 1 : va === vb ? 0 : -1
        if (comparers && comparers[column.id])
            compare = comparers[column.id]

        else if (typeof values[0] === 'string')
            values = values.map(x => x.toLowerCase())

        const compareValues = (a, b) => compare(values[a], values[b])
        let sortIndex = values.map((dummy, i) => i).sort(compareValues)

        if (sortDescending)
            sortIndex.reverse()

        return sortIndex
    }

    getInitialSortIndex() {
        const { sortColumn, columns } = this.props
        try {
            return (typeof sortColumn === typeof 0)
                ? this.getSortIndex(sortColumn, false)
                : columns[0].values.map((dummy, i) => i)
        }
        catch (e) {
            return []
        }
    }

    render() {

        const { setFirstRow, sortByColumn, setColumnWidth, clickCell } = this
        const { columns, renderers, comparers, style, className } = this.props
        const { columnWidths, sortIndex, firstRow, selectedRow, sortColumn, sortDescending } = this.state


        return <DiwaliGrid.Inner
            columns={columns}
            renderers={renderers}
            comparers={comparers}
            columnWidths={columnWidths}
            sortIndex={sortIndex}
            firstRow={firstRow}
            selectedRow={selectedRow}
            sortColumn={sortColumn}
            sortDescending={sortDescending}
            style={style}
            className={className}

            setFirstRow={setFirstRow}
            sortByColumn={sortByColumn}
            setColumnWidth={setColumnWidth}
            clickCell={clickCell}

        />

    }

}

DiwaliGrid.Inner = class extends React.Component {

    constructor(props) {
        super(props)

        this.setScrollTop = scrollTop => {
            const { rowHeight = DiwaliGrid.defaults.rowHeight, setFirstRow } = this.props
            const firstRow = Math.trunc(scrollTop / rowHeight)
            setFirstRow(firstRow)
        }

        this.onWheel = event => {
            const { rowHeight = DiwaliGrid.defaults.rowHeight } = this.props
            const scrollbar = this.scrollbar
            event.stopPropagation();
            event.preventDefault();

            const delta = event.nativeEvent.deltaY > 0 ? rowHeight : -rowHeight
            const scrollTop = this.scrollbar.scrollTop + delta;
            scrollbar.scrollTop = scrollTop;
            this.setScrollTop(scrollbar.scrollTop)
        }

        this.onScroll = event => {
            event.stopPropagation();
            event.preventDefault();
            this.setScrollTop(event.target.scrollTop)
        }

        this.refScrollbar = elem => {
            if (elem && elem !== this.scrollbar) {
                elem.addEventListener('scroll', this.onScroll, true)
                this.scrollbar = elem
            }
        }

        this.beginDrag = columnIndex => event => {
            const { columnWidths } = this.props

            event.dataTransfer
                .setData('react-grid', '')

            // hide the drag image
            event.dataTransfer
                .setDragImage(DiwaliGrid.invisibleImage, 100000, 100000)

            this.dragObject = {
                columnIndex,
                initialWidth: columnWidths[columnIndex],
                startX: event.pageX,
            }

            document.addEventListener('dragover', this.dragOver)
            document.addEventListener('dragend', this.dragEnd)
        }

        this.dragOver = event => {
            if (this.dragObject) {
                event.stopPropagation();
                event.preventDefault()

                const { setColumnWidth } = this.props
                const { columnIndex, startX, initialWidth } = this.dragObject
                setColumnWidth(columnIndex, initialWidth + event.pageX - startX)
            }
        }

        this.dragEnd = event => {
            if (this.dragObject) {
                event.stopPropagation();
                event.preventDefault()

                document.removeEventListener('dragover', this.dragOver)
                document.removeEventListener('dragend', this.dragEnd)
                this.dragObject = null
            }
        }

        this.clickColumnHead = columnIndex => event => {
            const { sortByColumn } = this.props
            event.stopPropagation()
            event.preventDefault()
            sortByColumn(columnIndex)
        }

        this.clickCell = (colIndex, rowIndex) => event => {
            const { clickCell } = this.props
            event.stopPropagation()
            event.preventDefault()
            clickCell(colIndex, rowIndex)
        }


        this.getClassNames = className => {
            const baseClass = (className ? className + " " : "default ") + " diwali-grid diwali-grid-"
            if (!this[baseClass]) {
                this[baseClass] = {
                    cell: baseClass + "cell",
                    selectedCell: baseClass + "cell selected",
                    column: baseClass + "column",
                    headCell: baseClass + "head-cell",
                    selectedHeadCell: baseClass + "head-cell selected",
                    selectedHeadCellDescending: baseClass + "head-cell selected descending",
                    scrollbar: baseClass + "scroll-bar",
                    container: baseClass + "container",
                    columnContainer: baseClass + "column-container",
                    scrollbarContent: baseClass + "scroll-bar-content",
                    title: baseClass + "title",
                    grip: baseClass + "grip",
                    head: baseClass + "head",
                    body: baseClass + "body",
                    cellText: baseClass + "cell-text",
                }
            }
            return this[baseClass];
        }
    }

    render() {

        const {
            firstRow = 0,
            selectedRow,
            renderedRowCount = DiwaliGrid.defaults.renderedRowCount,
            rowHeight = DiwaliGrid.defaults.rowHeight,
            scrollControlWidth = DiwaliGrid.defaults.scrollControlWidth,

            sortColumn,
            sortDescending,
            columns,
            renderers,
            columnWidths,
            sortIndex,
            style,
            className = '',

        } = this.props

        const {
            onWheel,
            refScrollbar,
            beginDrag,
            clickColumnHead,
            clickCell,
            getClassNames,
        } = this



        const totalWidth = columnWidths.reduce((a, b) => a + b, 0);

        const styleRowHeight = { height: rowHeight + 'px', lineHeight: rowHeight + 'px' }
        const styleTotalWidth = { width: totalWidth + 'px' }


        const valueIndex = sortIndex.slice(firstRow, firstRow + renderedRowCount)
        const dataHeight_px = ((columns[0].values.length + 2) * rowHeight) + 'px'

        const classes = getClassNames(className)
        const renderCell = (value, rowIndex, colIndex, renderer) =>
            <div key={rowIndex} className={rowIndex === selectedRow ? classes.selectedCell : classes.cell}
                onClick={clickCell(colIndex, rowIndex)}
                style={styleRowHeight}>
                {renderer(value)}
            </div>


        const defaultRenderer = text => <span className={classes.cellText}>{text}</span>

        const renderColumn = (column, colIndex) => {
            const renderer = renderers[column.id] || defaultRenderer
            return <div key={colIndex} className={classes.column}
                style={{ width: columnWidths[colIndex] + 'px' }}>
                {valueIndex.map(rowIndex => renderCell(column.values[rowIndex], rowIndex, colIndex, renderer))}
            </div>
        }

        const renderHeadCell = (column, index) =>
            <div key={index} className={
                index === sortColumn
                    ? sortDescending ? classes.selectedHeadCellDescending : classes.selectedHeadCell
                    : classes.headCell
            }
                onClick={clickColumnHead(index)}
                style={{ width: columnWidths[index] + 'px', height: (rowHeight-1) + 'px', lineHeight: (rowHeight-1) + 'px' } }>
                <div className={classes.title}
                    style={styleRowHeight}>
                    {column.title}
                </div>
                <div className={classes.grip}
                    style={styleRowHeight}
                    draggable="true"
                    onDragStart={beginDrag(index)}
                />
            </div>

        const scrollbarContent =
            <div className={classes.scrollbarContent}
                style={{ width: '1px', height: dataHeight_px }}>
            </div>

        return <div className={classes.container}
            style={style}
            onWheel={onWheel}>
            <div className={classes.columnContainer}
                style={{ right: scrollControlWidth }}>
                <div className={classes.head} style={styleTotalWidth}>
                    {columns.map(renderHeadCell)}
                </div>
                <div className={classes.body} style={styleTotalWidth}>
                    {columns.map(renderColumn)}
                </div>
            </div>
            <div className={classes.scrollbar}
                style={{ bottom: scrollControlWidth }}
                ref={refScrollbar}>
                {scrollbarContent}
            </div>
        </div>

    }

}

DiwaliGrid.invisibleImage = document.createElement("img")
DiwaliGrid.invisibleImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

DiwaliGrid.defaults = {
    initialWidth: 100,
    minWidth: 32,
    delay: 50,
    rowHeight: 24,
    scrollControlWidth: 14,
    renderedRowCount: 100,
}

export default DiwaliGrid