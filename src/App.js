import React from 'react';
import DiwaliGrid from './DiwaliGrid'
import './App.css';



class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      columns: [
        { id: '0', title: 'Row', initialWidth: 100 },
        { id: 'a', title: 'A', initialWidth: 100 },
        { id: 'b', title: 'B', },
      ]
    }

    let d = [...Array(200000).keys()]

    const shuffle = (array) => {
      array = array.slice()
      var i = 0
        , j = 0
        , temp = null
    
      for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
      }
      return array
    }

    this.state.columns[0].values = d
    this.state.columns[1].values = shuffle(d)
    this.state.columns[2].values = shuffle(d)

    this.renderers = {
      a: value => <div style={{ backgroundColor: 'rgba(0,255,0,0.2)' }}>{"[" + value + "]"}</div>
    }

    const compare = (a, b) => a > b ? 1 : a === b ? 0 : -1
    this.comparers = {
      a: (a, b) => -compare(a, b)
    }

  }


  render() {
    const style = {
      position: 'absolute',
      top: '100px',
      left: '100px',
      bottom: '100px',
      right: '100px',
    }

    const clickCell = (colIndex, rowIndex) => {
      let columns = this.state.columns.slice();
      let values =columns[colIndex].values.slice()
      values[rowIndex] = values[rowIndex] + 1
      columns[colIndex] = { ...(columns[colIndex]), values}
      this.setState({columns});
    } 

    return <DiwaliGrid
      className="test"
      columns={this.state.columns}
      renderers={this.renderers}
      comparers={this.comparers}
      style={style}
      clickCell={clickCell}
    />
  }
}

export default App;
